require "bundler/capistrano"
require "whenever/capistrano"
load "deploy/assets"


set :application, "planeta futebol"

default_run_options[:pty] = true
default_run_options[:shell] = false # This ensures that remote capistrano deployment does not fork a remote shell using command “sh -c”.

set :repository, "git@bitbucket.org:tbueno/planetafutebol.git"
set :scm, "git"
set :branch, "master"
ssh_options[:forward_agent] = true
set :deploy_via, :remote_cache # Perform fetch instead of full clone
set :git_enable_submodules, 1  # Fetch submodules

set :user, "tbueno"
set :use_sudo, false
set :scm_passphrase, "thi8busil"
set :deploy_to, '/home/tbueno/apps/vole.io'

# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

server "tbueno.com", :web, :app

set :whenever_command, "bundle exec whenever"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

#If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

## desc "precompile the assets"
#  task :precompile_assets, :roles => :web, :except => { :no_release => true } do
#    run "cd #{current_path}; rm -rf public/assets/*"
#    run "cd #{current_path}; RAILS_ENV=production bundle exec rake #{asset_env} assets:precompile"
#  end

