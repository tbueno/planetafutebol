Planetafutebol::Application.routes.draw do


  resources :clubs, :path => 'clubes', :only => [:index, :show] do
    resources :articles, :path => 'noticias'
  end

  match 'clube/:slug' => 'clubs#show', :as => 'clube'
  match 'clube/:slug/noticias/:title' => 'articles#show', :as => 'noticia'
  
  root :to => 'clubs#index'

  match 'sobre' => 'pages#sobre'

  match 'contato' => 'contact#new', :as => 'contact', :via => :get
  match 'contato' => 'contact#create', :as => 'contact', :via => :post
end
