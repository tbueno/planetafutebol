# encoding: UTF-8
#db   = conn["planetafutebol_#{Rails.env}"]
#db   = conn["app6865809"]


db = URI.parse(ENV['MONGOHQ_URL'])
db_name = db.path.gsub(/^\//, '')

db_connection = Mongo::Connection.new(db.host, db.port).db(db_name)
db_connection.authenticate(db.user, db.password) unless (db.user.nil? || db.user.nil?)


migrations = db_connection['migrations']
if migrations.find_one({file: __FILE__})
  puts "Skipping:  #{__FILE__}"
else
  puts "Migrating: #{__FILE__}"

  CLUBS = [
    { name: 'Cruzeiro', 
      website: 'www.cruzeiro.com.br',
      twitter: 'CruzeiroEC',
      sources: [
        {name: 'ESPN', website: 'http://www.espn.com.br/rss/cruzeiro', url: 'http://espn.estadao.com.br/rss/cruzeiro'},
        {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/35/cruzeiro/', url: 'http://www.gazetaesportiva.net/rss/35.xml'},
        {name: 'Lancenet', website: 'http://www.lancenet.com.br/cruzeiro', url: 'http://www.lancenet.com.br/rss/cruzeiro/'},
        {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/cruzeiro/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/cruzeiro/feed.rss'},
        {name: 'Canelada', website: 'http://canelada.com.br/category/cruzeiro/', url: 'http://feeds.feedburner.com/CaneladaCruzeiro'}
      ] 
    },
    { name: 'Atlético-Mg', 
      website: 'http://www.atletico.com.br',
      twitter: 'sitedogalo',
      sources: [
        {name: 'ESPN', website: 'http://www.espn.com.br/rss/atleticomineiro', url: 'http://espn.estadao.com.br/rss/atleticomineiro'},
        {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/15/atletico-mg/', url: 'http://www.gazetaesportiva.net/rss/15.xml'},
        {name: 'Lancenet', website: 'http://www.lancenet.com.br/atletico-mineiro', url: 'http://www.lancenet.com.br/rss/atletico-mineiro/'},
        {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/atletico-mg/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/atletico-mg/feed.rss'},
        {name: 'Canelada', website: 'http://canelada.com.br/category/atletico-mg/', url: 'http://feeds.feedburner.com/CaneladaAtletico-MG'}
      ] 
    }
  ]

  puts 'CREATING CLUBS:'
  CLUBS.each do |club|
    c = Club.where(name: club[:nome]).first
    unless c
      c = Club.create(name: club[:name], website: club[:website], twitter: club[:twitter])
      puts "\t - Criando o clube '#{c.name}'"
      club[:sources].each do |source|
        c.sources << Source.create(source)
      end
    end
  end

  migrations.insert({file: __FILE__})
  puts "\tDone!"
end
