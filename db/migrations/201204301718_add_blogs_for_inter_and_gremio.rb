# encoding: UTF-8

db = URI.parse(ENV['MONGOHQ_URL'])
db_name = db.path.gsub(/^\//, '')

db_connection = Mongo::Connection.new(db.host, db.port).db(db_name)
db_connection.authenticate(db.user, db.password) unless (db.user.nil? || db.user.nil?)
  
migrations = db_connection['migrations']
if migrations.find_one({file: __FILE__})
  puts "Skipping:  #{__FILE__}"
else


  CLUBS = [
    { name: 'Internacional',
      sources: [
        {name: 'Arena Vermelha', website: 'http://arenavermelha.blogspot.com.br/', url: 'feed://feeds.feedburner.com/blogspot/tdpfH'},
        {name: 'Blog Colorado', website: 'http://wp.kzuka.com.br/blogcolorado/', url: 'feed://br.clicesportes.feedsportal.com/c/33426/f/578725/index.rss'},
        {name: 'Blog Vermelho', website: 'http://bolavermelho.blogspot.com/feeds/posts/default', url: 'http://bolavermelho.blogspot.com/feeds/posts/default'},
        {name: 'Celeumas e Resenhas Coloradas', website: 'http://celeumacolorada.wordpress.com', url: 'http://celeumacolorada.wordpress.com/feed/'},
        {name: 'Supremacia Colorada', website: 'http://www.supremaciacolorada.com/feeds/posts/default', url: 'http://www.supremaciacolorada.com/feeds/posts/default'},
        {name: 'Beco do Sapulha', website: 'http://www.supremaciacolorada.com/feeds/posts/default', url: 'http://www.supremaciacolorada.com/feeds/posts/default'}
      ]
    },
    { name: 'Grêmio',
      sources: [
        {name: 'Grêmio Libertador', website: 'http://www.gremiolibertador.com', url: 'feed://www.gremiolibertador.com/feed/'},
        {name: 'Grêmio Copero', website: 'http://www.gremiocopero.com', url: 'http://www.gremiocopero.com/feed/'},
        {name: 'Grêmio 1983', website: 'http://gremio1983.blogspot.com', url: 'http://gremio1983.blogspot.com/feeds/posts/default'},
        {name: 'Imortal Tricolor', website: 'http://gremio1983.blogspot.com/feeds/posts/default', url: 'http://blogremio.blogspot.com/feeds/posts/default'},
        {name: 'Grêmio Arena', website: 'http://arenadogremio.blogspot.com/feeds/posts/default', url: 'http://arenadogremio.blogspot.com/feeds/posts/default'}
      ]
    }
  ]

  CLUBS.each do |club|
    model = Club.where(name: club[:name]).first
    puts "Adding sources to: #{model.name}"
    club[:sources].each do |source|
      puts "\t - #{source[:name]}"
      model.sources << Source.create(source)
    end
  end
  
  migrations.insert({file: __FILE__})
  puts "\tDone!"
end









