# encoding: UTF-8

db = URI.parse(ENV['MONGOHQ_URL'])
db_name = db.path.gsub(/^\//, '')

db_connection = Mongo::Connection.new(db.host, db.port).db(db_name)
db_connection.authenticate(db.user, db.password) unless (db.user.nil? || db.user.nil?)

migrations = db_connection['migrations']

if migrations.find_one({file: __FILE__})
  puts "Skipping:  #{__FILE__}"
else
  puts "Migrating: #{__FILE__}"

  CLUBS = [
    { name: 'São Paulo', 
      website: 'http://www.saopaulofc.net/',
      twitter: 'saopaulofc',
      sources: [
        {name: 'ESPN', website: 'http://www.espn.com.br/rss/saopaulo', url: 'http://espn.estadao.com.br/rss/saopaulo'},
        {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/29/sao-paulo/', url: 'http://www.gazetaesportiva.net/rss/29.xml'},
        {name: 'Lancenet', website: 'http://www.lancenet.com.br/sao-paulo', url: 'http://www.lancenet.com.br/rss/sao-paulo/'},
        {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/sao-paulo/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/sao-paulo/feed.rss'},
        {name: 'Canelada', website: 'http://canelada.com.br/category/sao-paulo/', url: 'feed://feeds.feedburner.com/caneladasaopaulo'}
      ] 
    },
    { name: 'Vasco', 
      website: 'http://www.vasco.com.br',
      twitter: 'crvascodagama',
      sources: [
        {name: 'ESPN', website: 'http://www.espn.com.br/rss/vasco', url: 'http://espn.estadao.com.br/rss/vasco'},
        {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/30/vasco/', url: 'http://www.gazetaesportiva.net/rss/30.xml'},
        {name: 'Lancenet', website: 'http://www.lancenet.com.br/vasco', url: 'http://www.lancenet.com.br/rss/vasco/'},
        {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/vasco/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/vasco/feed.rss'},
        {name: 'Canelada', website: 'http://canelada.com.br/category/vasco/', url: 'http://feeds.feedburner.com/CaneladaVasco'}
      ] 
    },
    { name: 'Palmeiras', 
       website: 'http://www.palmeiras.com.br',
       twitter: 'SitePalmeiras',
       sources: [
         {name: 'ESPN', website: 'http://www.espn.com.br/rss/palmeiras', url: 'http://espn.estadao.com.br/rss/palmeiras'},
         {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/24/palmeiras/', url: 'http://www.gazetaesportiva.net/rss/24.xml'},
         {name: 'Lancenet', website: 'http://www.lancenet.com.br/palmeiras', url: 'http://www.lancenet.com.br/rss/palmeiras/'},
         {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/palmeiras/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/palmeiras/feed.rss'},
         {name: 'Canelada', website: 'http://canelada.com.br/category/palmeiras/', url: 'http://feeds.feedburner.com/Caneladapalmeiras'}
       ] 
     },
    { name: 'Fluminense', 
       website: 'http://www.fluminense.com.br',
       twitter: 'OficialFlu',
       sources: [
         {name: 'ESPN', website: 'http://www.espn.com.br/rss/fluminense', url: 'http://espn.estadao.com.br/rss/fluminense'},
         {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/21/fluminense/', url: 'http://www.gazetaesportiva.net/rss/21.xml'},
         {name: 'Lancenet', website: 'http://www.lancenet.com.br/fluminense', url: 'http://www.lancenet.com.br/rss/fluminense/'},
         {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/fluminense/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/fluminense/feed.rss'},
         {name: 'Canelada', website: 'http://canelada.com.br/category/fluminense/', url: 'http://feeds.feedburner.com/Caneladafluminense'}
       ] 
     },
    { name: 'Botafogo', 
       website: 'http://www.botafogo.com.br',
       twitter: 'BotafogoOficial',
       sources: [
         {name: 'ESPN', website: 'http://www.espn.com.br/rss/botafogo', url: 'http://espn.estadao.com.br/rss/botafogo'},
         {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/18/botafogo/', url: 'http://www.gazetaesportiva.net/rss/18.xml'},
         {name: 'Lancenet', website: 'http://www.lancenet.com.br/botafogo', url: 'http://www.lancenet.com.br/rss/botafogo/'},
         {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/botafogo/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/botafogo/feed.rss'},
         {name: 'Canelada', website: 'http://canelada.com.br/category/botafogo/', url: 'http://feeds.feedburner.com/Caneladabotafogo'}
       ] 
     },
    { name: 'Santos', 
       website: 'http://www.santosfc.com.br',
       twitter: 'santosfc',
       sources: [
         {name: 'ESPN', website: 'http://www.espn.com.br/rss/santos', url: 'http://espn.estadao.com.br/rss/santos'},
         {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/27/santos/', url: 'http://www.gazetaesportiva.net/rss/27.xml'},
         {name: 'Lancenet', website: 'http://www.lancenet.com.br/santos', url: 'http://www.lancenet.com.br/rss/santos/'},
         {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/santos/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/santos/feed.rss'},
         {name: 'Canelada', website: 'http://canelada.com.br/category/santos/', url: 'http://feeds.feedburner.com/Caneladasantos'}
       ] 
     }
  ]

  puts 'CREATING CLUBS:'
  CLUBS.each do |club|
    c = Club.where(name: club[:nome]).first
    unless c
      c = Club.create(name: club[:name], website: club[:website], twitter: club[:twitter])
      puts "\t - Criando o clube '#{c.name}'"
      club[:sources].each do |source|
        c.sources << Source.create(source)
      end
    end
  end

  migrations.insert({file: __FILE__})
  puts "\tDone!"
end
