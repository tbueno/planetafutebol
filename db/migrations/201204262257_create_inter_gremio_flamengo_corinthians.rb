# encoding: UTF-8

db = URI.parse(ENV['MONGOHQ_URL'])
db_name = db.path.gsub(/^\//, '')

db_connection = Mongo::Connection.new(db.host, db.port).db(db_name)
db_connection.authenticate(db.user, db.password) unless (db.user.nil? || db.user.nil?)


migrations = db_connection['migrations']
if migrations.find_one({file: __FILE__})
  puts "Skipping:  #{__FILE__}"
else
  puts "Migrating: #{__FILE__}"

  CLUBS = [
    { name: 'Internacional', 
      website: 'http:///www.internacional.com.br',
      twitter: 'SCInternacional',
      sources: [
        {name: 'ESPN', website: 'http://www.espn.com.br/rss/internacional', url: 'http://espn.estadao.com.br/rss/internacional'},
        {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/89/inter-rs/', url: 'http://www.gazetaesportiva.net/rss/89.xml'},
        {name: 'Lancenet', website: 'http://www.lancenet.com.br/internacional/', url: 'http://www.lancenet.com.br/rss/internacional/'},
        {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/internacional/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/internacional/feed.rss'},
        {name: 'Canelada', website: 'http://canelada.com.br/category/internacional/', url: 'http://feeds.feedburner.com/CaneladaInternacional'}
      ] 
    },
    
    {
    name: 'Grêmio',
    website: 'http://www.gremio.net',
    twitter: 'gremiooficial',
    sources: [
        {name: 'ESPN', website: 'http://www.espn.com.br/rss/gremio', url: 'http://espn.estadao.com.br/rss/gremio'},
        {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/22/gremio/', url: 'http://www.gazetaesportiva.net/rss/22.xml'},
        {name: 'Lancenet', website: 'http://www.lancenet.com.br/gremio/', url: 'http://www.lancenet.com.br/rss/gremio/'},
        {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/gremio/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/gremio/feed.rss'},
        {name: 'Canelada', website: 'http://canelada.com.br/category/gremio/', url: 'http://feeds.feedburner.com/CaneladaGremio'}
      ]
    },
    
    {
    name: 'Flamengo',
    website: 'http://www.flamengo.com.br',
    twitter: 'cr_flamengo',
    sources: [
        {name: 'ESPN', website: 'http://www.espn.com.br/rss/flamengo', url: 'http://espn.estadao.com.br/rss/flamengo'},
        {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/20/flamengo/', url: 'http://www.gazetaesportiva.net/rss/20.xml'},
        {name: 'Lancenet', website: 'http://www.lancenet.com.br/flamengo/', url: 'http://www.lancenet.com.br/rss/flamengo/'},
        {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/flamengo/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/flamengo/feed.rss'},
        {name: 'Canelada', website: 'http://canelada.com.br/category/flamengo/', url: 'http://feeds.feedburner.com/CaneladaFlamengo'}
      ]
    },

    {
    name: 'Corinthians',
    website: 'http://www.corinthians.com.br',
    twitter: 'SiteCorinthians',
    sources: [
        {name: 'ESPN', website: 'http://www.espn.com.br/rss/corinthians', url: 'http://espn.estadao.com.br/rss/corinthians'},
        {name: 'Gazeta Esportiva', website: 'http://www.gazetaesportiva.net/canal/19/corinthians/', url: 'http://www.gazetaesportiva.net/rss/19.xml'},
        {name: 'Lancenet', website: 'http://www.lancenet.com.br/corinthians/', url: 'http://www.lancenet.com.br/rss/corinthians/'},
        {name: 'Globoesporte', website: 'http://globoesporte.globo.com/futebol/times/corinthians/', url: 'http://globoesporte.globo.com/dynamo/semantica/editorias/plantao/futebol/times/corinthians/feed.rss'},
        {name: 'Canelada', website: 'http://canelada.com.br/category/corinthians/', url: 'http://feeds.feedburner.com/CaneladaCorinthians'}
      ]
    }
  ]


  puts 'EMPTYING THE MONGODB DATABASE...'
  Club.destroy_all
  Source.destroy_all
  Article.destroy_all


  puts 'CREATING CLUBS:'
  CLUBS.each do |club|
    c = Club.where(name: club[:nome]).first
    unless c
      c = Club.create(name: club[:name], website: club[:website], twitter: club[:twitter])
      puts "\t - Criando o clube '#{c.name}'"
      club[:sources].each do |source|
        c.sources << Source.create(source)
      end
    end
  end

  migrations.insert({file: __FILE__})
  puts "\tDone!"
end
