# encoding: UTF-8

db = URI.parse(ENV['MONGOHQ_URL'])
db_name = db.path.gsub(/^\//, '')

db_connection = Mongo::Connection.new(db.host, db.port).db(db_name)
db_connection.authenticate(db.user, db.password) unless (db.user.nil? || db.user.nil?)

migrations = db_connection['migrations']

if migrations.find_one({file: __FILE__})
  puts "Skipping:  #{__FILE__}"
else
  puts "Migrating: #{__FILE__}"
  puts 'adding slug'

  Club.all.each do |club|
     puts club.generate_slug
  end

  migrations.insert({file: __FILE__})
  puts "\tDone!"
end
