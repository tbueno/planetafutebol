class Article
  include Mongoid::Document
  field :title, :type => String
  field :summary, :type => String
  field :content, :type => String
  field :author, :type => String
  field :url, :type => String
  field :slug, :type => String
  field :published_at, :type => Time

  validates_uniqueness_of :title, message: "Title thould be unique"
  validates_uniqueness_of :content
  validates_presence_of :content

  belongs_to :club
  belongs_to :source
  
  before_save :slugify!
  
  def slugify!
    self.slug = title.downcase.strip.gsub(' ', '-').mb_chars.normalize(:kd).gsub(/[^\-x00-\x7F]/n, '').to_s
  end

  index(
    [
      [:title, Mongo::ASCENDING]
    ],
    unique: true
  )

end
