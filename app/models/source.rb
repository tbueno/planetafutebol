class Source
  include Mongoid::Document
  field :name, type: String
  field :website, type: String
  field :url, type: String
  field :parsed_at, :type => Time, default: (Time.now - 5.days) 
  belongs_to :club
end
