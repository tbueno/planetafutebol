class Club
  include Mongoid::Document
  field :name, :type => String
  field :website, :type => String
  field :twitter, :type => String
  field :slug, :type => String

  validates_presence_of :name, :website
  validates_uniqueness_of :name, :website

  has_many :sources
  has_many :articles

  before_create :generate_slug

  index([
    [:name, Mongo::ASCENDING]
  ],
    unique: true
  )

  def generate_slug
    self.slug = small_name 
  end

  private
  def small_name
    name.downcase.mb_chars.normalize(:kd).gsub(/[^\-x00-\x7F]/n, '').to_s
  end

end
