class NotificationsMailer < ActionMailer::Base
  default :from => "noreply@vole.io"
  default :to => "contato@vole.io"

  def new_message(message)
    @message = message
    mail(:subject => "[YourWebsite.tld] #{message.subject}")
  end
end
