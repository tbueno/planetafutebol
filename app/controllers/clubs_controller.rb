class ClubsController < ApplicationController

  def index
    @clubs = Club.all
  end

  def show
    @club = Club.where(:slug => params[:slug]).first
    @articles = @club.articles.desc(:published_at).page(params[:page])
    respond_to do |format|
      format.html
      format.json
      format.rss { render :layout => false }
    end
  end

end
