require File.expand_path('../../../vendor/tools/parser/lib/parser', __FILE__)

class FirstWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(club_id)
    club = Club.find(club_id)
    Parser::Collector.parse Feedzirra::Feed, club
  end
end
