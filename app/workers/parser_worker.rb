require File.join(Rails.root, 'vendor/tools/parser/lib/parser')

class ParserWorker

  @queue = "parser"

  def self.perform(club_id)
    puts "Processing job on club #{club_id}. Workou"
    Parser::Collector.parse Feedzirra::Feed
  end

end
