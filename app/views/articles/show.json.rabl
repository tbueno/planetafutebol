object @article

attributes :title, :content
node(:source_url) { @article.source.website }
