object @club

attributes :name

child @articles do
  attributes :title, :published_at, :summary
  node(:path){ |article| club_article_url(@club.name, article.slug) + '.json' }
end
