def introduction_for(article)
  intro = "<p>Fonte: "
  intro += link_to article.source.name, article.url
  intro += "</p>"
end

def conclusion_for(article)
  conclusion = tag(:hr)
  link = link_to("Mais notícias sobre #{article.club.name}", clube_path(article.club.name))
  conclusion += content_tag(:p, link)


end


xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "Vole.io - #{@club.name}"
    xml.description "Notícias sobre o #{@club.name}"
    xml.link clube_path

    @articles.each do |article|
      xml.item do
        xml.title article.title
        xml.description "#{introduction_for(article)} #{article.content} #{conclusion_for(article)}"
        xml.pubDate article.published_at.to_s(:rfc822)
        xml.link club_article_path(@club.name, article.slug)
      end
    end
  end
end
