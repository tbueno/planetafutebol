# encoding: UTF-8
module ApplicationHelper
  def site_title
    title = ""
    resource = main_resource
    return title << "Notícias atualizadas sobre futebol | Vole.io" unless resource
    if resource.is_a?(Club)
      title << "Notícias sobre o clube #{resource.name}" 
    elsif resource.is_a?(Article)
      title << "#{resource.title}"
    end
    title << ' | Vole.io'
    title
  end

  def breadcrumbs_for(resource)
    link = content_tag(:a, 'Home', href: '/')
    link << content_tag('span', '/', :class => 'divider')
    breadcrumb = content_tag(:li, link )
    if resource.is_a?(Club)
      breadcrumb << content_tag('li', resource.name, class: 'active')
    else
      breadcrumb << content_tag('li', resource.club.name)
    end
    breadcrumb
  end

  def image_for(club)
    return "" unless club.name
    name = club.name.downcase.strip.gsub(' ', '-').mb_chars.normalize(:kd).gsub(/[^\-x00-\x7F]/n, '').to_s
    "#{name}.png"
  end

  private
  def main_resource
    @article || @club
  end

end
