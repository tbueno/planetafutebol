desc "This task is called by the Heroku scheduler add-on"
task :update_feed => :environment do
    puts "Parsin feeds..."
    require File.expand_path('../../../vendor/tools/parser/lib/parser', __FILE__)
    require File.expand_path('../../../app/models/club', __FILE__)
    require File.expand_path('../../../app/models/source', __FILE__)
    require File.expand_path('../../../app/models/article', __FILE__)
    Club.all.each do |club|
      Parser::Collector.parse Feedzirra::Feed, club
    end
    puts "done."
end

