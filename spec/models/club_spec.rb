# encoding: UTF-8
require 'spec_helper'

describe Club do

  describe "validations" do

    describe "name" do
      subject { Club.new }
      
      it "should exist" do
        subject.should_not be_valid
        subject.name = "Foo"
        subject.website = "http://foo"
        subject.should be_valid
      end

      it "should be unique" do
        subject.name = "foo"
        subject.save
        c = Club.new(:name => 'foo')
        c.should_not be_valid
      end

      it "generates slug" do
        Club.create(name: 'São paulo', website: "http://foo.com").slug.should == 'saopaulo'
      end
    end
  end
end

