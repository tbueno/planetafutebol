# encoding: UTF-8

require 'spec_helper'

describe Article do
  it 'makes sure title is unique' do
    Article.create(title: "My title", content: 'foo')
    a = Article.new(title: "My title", content: 'bar')
    a.save
    a.invalid?(:name).should be_true
  end

  it 'makes sure content is not empty' do
    a = Article.new(title: 'Title')
    a.should_not be_valid
    a.invalid?(:content).should be_true
  end

  describe 'slugify' do
    it 'removes spaces' do
      a = Article.create(title: 'foo bar', content: "content")
      a.slug.should == 'foo-bar'
    end

    it 'removes accents' do
      a = Article.create(title: 'fôo bár', content: "content")
      a.slug.should == 'foo-bar'
    end

  end
end

