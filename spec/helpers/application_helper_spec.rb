# encoding: UTF-8
require 'spec_helper'

describe ApplicationHelper do

  describe 'Site Title' do
    
    let(:suffix) { 'Vole.io' }
    
    let(:club) do 
      Club.new(name: "Inter")
    end

    let(:article) do 
      Article.new(title: "Title")
    end
    
    context "in the home page" do
      it "Uses the site slogan" do
        helper.site_title.should == "Notícias atualizadas sobre futebol | #{suffix}"
      end
    end

    context "in a club page" do
      it "uses prefix + club name with news callout" do
        helper.stub(main_resource: club)
        helper.site_title.should == "Notícias sobre o clube #{club.name} | #{suffix}"
      end
    end

    context "in an article page" do
      it " uses prefix + article title" do
        helper.stub(main_resource: article)
        helper.site_title.should == "#{article.title} | #{suffix}"
      end
    end
  end
  
  describe "image for club" do
    it "generates image file name base on club" do
      helper.image_for(Club.new(name: 'Inter')).should == "inter.png"
    end

    it 'return empty string when club name is nil' do
      helper.image_for(Club.new()).should == ""
    end

    it 'removes accents' do
      helper.image_for(Club.new(name: "Grêmio")).should == "gremio.png"
    end

    it 'replaces spaces for dashes' do
      helper.image_for(Club.new(name: 'São Paulo')).should == "sao-paulo.png"
    end
  end

  describe "breadcrumbs" do

    let(:club){Club.new(name: 'inter')}
    let(:article) {Article.new(club: club)}

    it "generates breacrumbs for club" do
      helper.breadcrumbs_for(club).should == "<li><a href=\"/\">Home</a><span class=\"divider\">/</span></li><li class=\"active\">inter</li>"
    end

    it "generates breacrumbs for club" do
      helper.breadcrumbs_for(article).should == "<li><a href=\"/\">Home</a><span class=\"divider\">/</span></li><li>inter</li>"
    end
  end
  
end
