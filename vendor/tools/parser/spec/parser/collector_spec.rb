require 'spec_helper'

describe Parser::Collector do
  
  let(:sources) do
    source = mock
    source.stub(:url => '', :parsed_at => Time.now, :name => 'foo', :articles => [], :save => true )
    [source]
  end
 
 # let(:feed) do
 #  feed = mock
 # end

 
 # let(:entry) do
 #      entry = mock(Feedzirra::Parser::RSSEntry)
 # end

  describe "parse" do
    
    let(:parser) do
      parser = mock
    end

    let(:club) do
      club = mock
    end

    it " a club" do 
      described_class.should_receive(:parse_club).with(parser, club)
      described_class.parse(parser, club)
    end

    it "more then one club" do
      class Club;end
      Club.should_receive(:all).and_return([club])
      described_class.should_receive(:parse_club).with(parser, club)
      described_class.parse(parser)
    end

  end
  
  describe "parse club" do

    let(:source_with_news) do
      stub(:parsed_at => Time.now - 2.days, :url => "http")
    end

    let(:source_without_news) { stub(:parsed_at => Time.now, :url => "http") }

    let(:feed) { stub(:last_modified => Time.now - 1.day) }

    let(:parser) {mock}

    it "calls get the feed from source" do
      club = stub(:sources => [stub(:url => 'http://source')])
      described_class.should_receive(:get_feed).with(parser, 'http://source')
      described_class.parse_club(parser, club)
    end
    
    context "with new articles" do
      it "saves entries" do
        described_class.should_receive(:get_feed).with(parser, source_with_news.url).and_return(feed)
        club = stub(:sources => [source_with_news])
        described_class.should_receive(:save_entries).with(club, source_with_news, feed)
        described_class.parse_club(parser, club)
      end
    end


    context "without new articles" do
      it "doesn't save articles" do
        described_class.should_receive(:get_feed).with(parser, source_without_news.url).and_return(feed)
        club = stub(:sources => [source_without_news])
        described_class.should_not_receive(:save_entries)
        described_class.parse_club(parser, club)
      end

    end
  end

   describe "save entries" do

   end

end
