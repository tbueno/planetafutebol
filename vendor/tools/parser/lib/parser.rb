# encoding: utf-8 
require "bundler/setup"
require 'mongoid'
require 'feedzirra'
require 'yaml'

module Parser

 Mongoid.load!('config/mongoid.yml')

  autoload :Collector, File.join(File.dirname(__FILE__), 'parser', 'collector')
  autoload :Espn, File.join(File.dirname(__FILE__), 'parser', 'espn')
  autoload :Lancenet, File.join(File.dirname(__FILE__), 'parser', 'lancenet')
  autoload :GloboEsporte, File.join(File.dirname(__FILE__), 'parser', 'globo_esporte')
  autoload :GazetaEsportiva, File.join(File.dirname(__FILE__), 'parser', 'gazeta_esportiva')
  autoload :FinalSports, File.join(File.dirname(__FILE__), 'parser', 'final_sports')
#  autoload :Uol, File.join(File.dirname(__FILE__), 'parser', 'uol')
end

Feedzirra::Feed.add_feed_class Parser::Espn
Feedzirra::Feed.add_feed_class Parser::GazetaEsportiva
Feedzirra::Feed.add_feed_class Parser::Lancenet
Feedzirra::Feed.add_feed_class Parser::GloboEsporte
Feedzirra::Feed.add_feed_class Parser::FinalSports



#Feedzirra::Feed.add_feed_class Parser::Uol
#Parser::Collector.parse Feedzirra::Feed

