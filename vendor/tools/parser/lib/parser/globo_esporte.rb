require 'open-uri'
require 'action_view'

module Parser    
    class GloboEsporteEntry < Feedzirra::Parser::RSSEntry
      include ActionView::Helpers::SanitizeHelper 
      include SAXMachine
      include Feedzirra::FeedEntryUtilities

      def content
        return @content if @content
        page = Nokogiri::HTML(open(url)).css('div#materia-letra').first
        @content = page.inner_html ? sanitize(page.inner_html, :attributes => %w(alt src title href)) : ''
      end

      def summary
        sanitize @summary, :tags => %w(p)
      end

      def author
        'globoesporte.com'
      end
    end



    class GloboEsporte 
      include SAXMachine
      include Feedzirra::FeedUtilities

      element :title

      element :link, :as => :url
      elements :item , :as => :entries, :class => GloboEsporteEntry

      attr_accessor :feed_url

      def self.able_to_parse?(xml)
         (/globoesporte.globo.com/ =~ xml)
      end

    end
end

