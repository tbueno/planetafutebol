require 'open-uri'
module Parser    
    class FinalSportsEntry < Feedzirra::Parser::RSSEntry
      include SAXMachine
      include Feedzirra::FeedEntryUtilities

      def content
        return @content if @content
        page = Nokogiri::HTML(open(url.strip)).css('div#noticia').first

        @content = sanitize(page.inner_html, %w(p))
      end
    end

    class FinalSports
      include SAXMachine
      include Feedzirra::FeedUtilities

      element :title

      element :link, :as => :url
      elements :item , :as => :entries, :class => FinalSportsEntry

      attr_accessor :feed_url

      def self.able_to_parse?(xml)
         (/finalsports/ =~ xml)
      end
    end
end
