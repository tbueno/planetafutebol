# encoding: utf-8 
module Parser

  class Collector
    def self.parse(parser, club = nil)
      if club
        parse_club(parser, club)
      else
        Club.all.each do |club|
          parse_club(parser, club)
        end
      end
    end

    def self.parse_club(parser, club)
      club.sources.each do |source|
        begin
          feed = get_feed(parser, source.url)
          next if feed.is_a?(Fixnum)
          save_entries(club,source,feed) unless (feed.last_modified < source.parsed_at)
        rescue
          next
        end
      end
    end

    def self.get_feed(parser, url)
      parser.fetch_and_parse url
    end

    def self.save_entries(club, source, feed)
      puts "Fetching content from: #{source.name} - (#{source.url})"
      feed.entries.each do |entry|
        unless entry.published
          entry.published = Time.now
        end

        if entry.published > source.parsed_at
          entry = force_utf8(entry)
          article = Article.new(:title=> entry.title, 
                                         :published_at => entry.published, 
                                         :source => source,
                                         :content => entry.content,
                                         :url => entry.url,
                                         :author => entry.author,
                                         :summary => entry.summary)
          club.articles << article
        end
      end
      source.parsed_at = Time.now
      source.save
    end

    private
    def self.force_utf8(entry)
      entry.content =  entry.content.encode
      entry.summary = entry.summary.encode
      entry
    end

  end
end


