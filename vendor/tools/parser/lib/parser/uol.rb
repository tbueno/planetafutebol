require 'open-uri'

module Parser    
    class UolEntry < Feedzirra::Parser::RSSEntry
      include SAXMachine
      include Feedzirra::FeedEntryUtilities

      def content
        @content ||= page.css('div#texto').first.inner_html
      end

      def title
        require 'debug'
        puts ''
      end

      def published
        require 'debug'
        puts ""
        
      end

      private
      def page
        @page ||= Nokogiri::HTML(open(url))
      end
    end



    class Uol
      include SAXMachine
      include Feedzirra::FeedUtilities

      element :title

      element :link, :as => :url
      elements :item , :as => :entries, :class => UolEntry

      attr_accessor :feed_url

      def self.able_to_parse?(xml)
         (/uol.com.br/ =~ xml)
      end

    end
end

