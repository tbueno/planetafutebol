require 'open-uri'
module Parser    
    class EspnEntry < Feedzirra::Parser::RSSEntry
      include SAXMachine
      include Feedzirra::FeedEntryUtilities

      def content
        return @content if @content
        page = Nokogiri::HTML(open(url)).css('div.texto_noticia').first
        @content = page.inner_html
      end
    end



    class Espn
      include SAXMachine
      include Feedzirra::FeedUtilities

      element :title

      element :link, :as => :url
      elements :item , :as => :entries, :class => EspnEntry

      attr_accessor :feed_url

      def self.able_to_parse?(xml)
         (/xmlns:espn/ =~ xml)
      end

    end
end

