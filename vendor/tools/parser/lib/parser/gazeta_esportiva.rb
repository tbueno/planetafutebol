require 'open-uri'
module Parser    

    class GazetaEsportivaEntry < Feedzirra::Parser::RSSEntry
      include SAXMachine
      include Feedzirra::FeedEntryUtilities

      def content
        return @content if @content        
        begin
          page = Nokogiri::HTML(open(url)).css('article').first
        rescue OpenURI::HTTPError
          puts "Unable to fetch content from: #{url}"
          return
        end
        @content = page.inner_html
      end
    end



    class GazetaEsportiva
      include SAXMachine
      include Feedzirra::FeedUtilities

      element :title

      element :link, :as => :url
      elements :item , :as => :entries, :class => GazetaEsportivaEntry

      attr_accessor :feed_url

      def self.able_to_parse?(xml)
         (/gazetaesportiva.net/ =~ xml)
      end

    end
end
