require 'open-uri'
module Parser    
    class LancenetEntry < Feedzirra::Parser::RSSEntry
      include SAXMachine
      include Feedzirra::FeedEntryUtilities

      def content
        return @content if @content

        begin
          page = Nokogiri::HTML(open(url)).css('div.news-body').first
        rescue

        end

        @content = page.inner_html if page
      end
    end



    class Lancenet
      include SAXMachine
      include Feedzirra::FeedUtilities

      element :title

      element :link, :as => :url
      elements :item , :as => :entries, :class => LancenetEntry

      attr_accessor :feed_url

      def self.able_to_parse?(xml)
         (/www.lancenet.com.br/ =~ xml)
      end

    end
end

